# Satelite_lite_station

#### 介绍
便携式卫星追踪器,使用arduino mage2560为基础开发

#### 软件架构
软件架构说明
软件使用arduino实现,在windows10 x64下测试成功
硬件环境:
1.  arduino mage2560 r3开发板 1个
2.  维特智能JY-GPSIMU模块 1个
3.  HC-05蓝牙模块 1个
4.  pst326云台 1个
5.  UV段天线系统及固定措施 1套
6.  ssd1306 0.96寸OLED屏 1个
7.  按键 2个
6.  12v转9V模块 1个
9.  三脚架 1个
10. Sdrplay 1个
11. PC电脑 1太
12. 面包板和杜邦线 若干


#### 安装教程f

以下软件均为windows版本,并默认路径安装

1.  ino文件直接使用arduino IDE直接编译
2.  fzz接线图文件使用Fritzing软件打开,安装接线图安装硬件模块
3.  安装最新hamlib,下载地址https://hamlib.github.io/
4.  安装最新SDRuno,下载地址https://www.sdrplay.com/downloads/
5.  安装最新gpredict,下载地址https://github.com/csete/gpredict/releases/download/v2.2.1/gpredict-win32-2.2.1.zip
5.  多普勒频移自动校准,须安装Virtual Serial Port,下载地址https://www.virtual-serial-port.org/downloads.html

#### 使用说明

1.  将硬件设备按照接线图安装好
2.  可以使用usb直接连接arduino控制或通过蓝牙控制
3.  接电后pst326云台将开始自检,左右各移动到限位块一次
6.  hamlib连接旋转器,运行脚本"C:\Program Files (x86)\hamlib-w64-3.3\bin\rotctld.exe" -m 202 -vvv -s 9600 -r com6<br/>
其中com6为与arduino通信的端口,可以是usb的或者是蓝牙的(传出),无错误提示表明连接成功
4.  安装Virtual Serial Port,须配置两个端口,这里举例com20和com21,后面的脚本端口根据实际情况设置
5.  打开SDRuno连接sdruno软件,sdruno软件RX CONTROL面板->SETT.->CAT-COM DEVICE(COM20) 勾选ENABLE和RX<br/>
运行脚本"C:\Program Files (x86)\hamlib-w64-3.3\bin\rigctld.exe" -m 202 -vvv -s 9600 -r com21
6.  打开gpredict<br/>
(1)更新星历和接受参数<br/>
Edit->Update TLE data from network<br/>
Edit->Update transponder data<br/>
(2)设置地面站参数<br/>
Edit->Genral->Ground Station->Add new->填入屏幕上获取的数据<br/>
name起个名字 latitude(la)维度 longtitude(lo)经度 altitude(al)高度<br/>
(3)添加旋转器<br/>
Edit->Genral->Interfaces->Rotators->Add new<br/>
一般默认参数,其中min和maxAZ指的是方位角范围,可以根据实际填写<br/>
(3)添加接受机<br/>
Edit->Genral->Interfaces->Radios->Add new<br/>
一般默认参数,可以根据实际填写<br/>
(4)选卫星<br/>
窗口右上角有个很小的向下的倒三角,下面所指的选项都在这里<br/>
选项->configure->选择你需要的卫星<br/>
选项->AutoTrack勾选,可以快速帮你追踪最近可见的卫星<br/>
选项->Antenna Control->Track->Engage,此时天线会移动,窗口会显示天线的位置(十字表示)<br/>
建议将Tolerance参数设为0.1,这样跟踪会更精确<br/>
选项->Radio Control->Track->Engage,若卫星可见,sdrplay将自动跳转频率<br/>
target下除了可以设置卫星外,还可以设置卫星不同的频率<br/>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


